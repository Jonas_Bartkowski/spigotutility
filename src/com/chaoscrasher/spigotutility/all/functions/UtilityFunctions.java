package com.chaoscrasher.spigotutility.all.functions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.material.Bed;
import org.bukkit.potion.PotionEffect;

public class UtilityFunctions 
{
	public static boolean isABetweenBAndC(Location a, Location b, Location c)
	{
		if (a.getWorld().equals(b.getWorld()) && b.getWorld().equals(c.getWorld()))
		{
			double xa = a.getX(), ya = a.getY(), za = a.getZ();
			double xb = b.getX(), yb = b.getY(), zb = b.getZ();
			double xc = c.getX(), yc = c.getY(), zc = c.getZ();
			
			boolean xt = ((xa >= xb && xa <=  xc) || (xa >= xc && xa <= xb));
			boolean yt = ((ya >= yb && ya <=  yc) || (ya >= yc && ya <= yb));
			boolean zt = ((za >= zb && za <=  zc) || (za >= zc && za <= zb));
			
			return (xt && yt && zt);
		}
		else
		{
			return false;
		}
	}
	
	public static boolean isABetweenBAndC_ignore_z(Location a, Location b, Location c)
	{
		System.out.println("Checking if ");
		System.out.println(a);
		System.out.println("is between");
		System.out.println(b);
		System.out.println("and");
		System.out.println(c);
		if (a.getWorld().equals(b.getWorld()) && b.getWorld().equals(c.getWorld()))
		{
			double xa = a.getX(), ya = a.getY();
			double xb = b.getX(), yb = b.getY();
			double xc = c.getX(), yc = c.getY();
			
			boolean xt = ((xa >= xb && xa <=  xc) || (xa >= xc && xa <= xb));
			boolean yt = ((ya >= yb && ya <=  yc) || (ya >= yc && ya <= yb));
			System.out.println("x is "+(xt ? "within " : "outside of ")+" the arena!");
			System.out.println("y is "+(yt ? "within " : "outside of ")+" the arena!");
			return (xt && yt);
		}
		else
		{
			return false;
		}
	}
	
	public static boolean isABetweenBAndC_ignore_y(Location a, Location b, Location c)
	{
		System.out.println("Checking if ");
		System.out.println(a);
		System.out.println("is between");
		System.out.println(b);
		System.out.println("and");
		System.out.println(c);
		if (a.getWorld().equals(b.getWorld()) && b.getWorld().equals(c.getWorld()))
		{
			double xa = a.getX(), za = a.getZ();
			double xb = b.getX(), zb = b.getZ();
			double xc = c.getX(), zc = c.getZ();
			
			boolean xt = ((xa >= xb && xa <=  xc) || (xa >= xc && xa <= xb));
			boolean zt = ((za >= zb && za <=  zc) || (za >= zc && za <= zb));
			System.out.println("x is "+(xt ? "within " : "outside of ")+" the arena!");
			System.out.println("y is "+(zt ? "within " : "outside of ")+" the arena!");
			return (xt && zt);
		}
		else
		{
			return false;
		}
	}
	
	public static String locationToString(Location loc)
	{
		if (loc == null)
			return null;
		
		return loc.getWorld().getName()+";"+loc.getX()+";"+loc.getY()+";"+loc.getZ()+";";
	}
	
	public static Location getLocationFromString(String locStr)
	{
		if (locStr == null || locStr.equalsIgnoreCase("null"))
			return null;
		
		String[] data = locStr.split(";");
		return new Location(Bukkit.getWorld(data[0]), Double.valueOf(data[1]), Double.valueOf(data[2]), Double.valueOf(data[3]));
	}
	
	public static Location getTwinLocation(Bed b, Block bloc)
	{
        if(b.isHeadOfBed())
        {
            return bloc.getRelative(b.getFacing().getOppositeFace()).getLocation();
        }
        else
        {
            return bloc.getRelative(b.getFacing()).getLocation();
        }
    }
	
	public static void putPlayerIntoStandardCondition(Player player)
	{
		Collection<PotionEffect> pes = player.getActivePotionEffects();
		for (PotionEffect pe: pes)
		{
			player.removePotionEffect(pe.getType());
		}
		player.setHealth(20);
		player.setFoodLevel(20);
	}
	
	public static void placeBed(Location bedHeadLocation, BlockFace bedFacing)
	{
		Block bedHeadBlock = bedHeadLocation.getBlock();
        Block bedFootBlock = bedHeadBlock.getRelative(bedFacing.getOppositeFace());
     
        BlockState bedFootState = bedFootBlock.getState();
        bedFootState.setType(Material.BED_BLOCK);
        Bed bedFootData = new Bed(Material.BED_BLOCK);
        bedFootData.setHeadOfBed(false);
        bedFootData.setFacingDirection(bedFacing);
        bedFootState.setData(bedFootData);
        bedFootState.update(true);

        BlockState bedHeadState = bedHeadBlock.getState();
        bedHeadState.setType(Material.BED_BLOCK);
        Bed bedHeadData = new Bed(Material.BED_BLOCK);
        bedHeadData.setHeadOfBed(true);
        bedHeadData.setFacingDirection(bedFacing);
        bedHeadState.setData(bedHeadData);
        bedHeadState.update(true);
	}
	
	public static BlockFace getPlayerFacing(Player player, boolean onlyNESW)
	{
		 double rot = (player.getLocation().getYaw() - 90) % 360;
         if (rot < 0) 
         {
                rot += 360.0;
         }	
         
         if (!onlyNESW)
         {
	         if (0 <= rot && rot < 22.5) {
	                 return BlockFace.NORTH;
	         } else if (22.5 <= rot && rot < 67.5) {
	                 return BlockFace.NORTH_EAST;
	         } else if (67.5 <= rot && rot < 112.5) {
	                 return BlockFace.EAST;
	         } else if (112.5 <= rot && rot < 157.5) {
	                 return BlockFace.SOUTH_EAST;
	         } else if (157.5 <= rot && rot < 202.5) {
	                 return BlockFace.SOUTH;
	         } else if (202.5 <= rot && rot < 247.5) {
	                 return BlockFace.SOUTH_WEST;
	         } else if (247.5 <= rot && rot < 292.5) {
	                 return BlockFace.WEST;
	         } else if (292.5 <= rot && rot < 337.5) {
	                 return BlockFace.NORTH_WEST;
	         } else if (337.5 <= rot && rot < 360.0) {
	                 return BlockFace.NORTH;
	         } else {
	                 return null;
	         }
         }
         else
         {
        	 if (0 <= rot && rot < 67.5) 
        	 {
	                 return BlockFace.NORTH;
	         } 
        	 else if (67.5 <= rot && rot < 157.5) 
        	 {
	                 return BlockFace.EAST;
	         } 
        	 else if (157.5 <= rot && rot < 247.5) 
        	 {
	                 return BlockFace.SOUTH;
	         } 
        	 else if (247.5 <= rot && rot < 337.5) 
        	 {
	                 return BlockFace.WEST;
	         } 
        	 else if (337.5 <= rot && rot < 360.0) 
        	 {
	                 return BlockFace.NORTH;
	         } 
        	 else 
	         {
	                 return null;
	         }
         }
	}
	
	public static void clearItems(World world, Location betweenHere, Location andHere)
	{
		clearEntities(world, betweenHere, andHere, Item.class);
	}
	
	public static void clearEntities(World world, int xs, int ys, int zs, int xe, int ye, int ze, Class... ignoreThese)
	{
        clearEntities(world, new Location(world,xs,ys,zs), new Location(world,xs,ys,zs), ignoreThese);
	}
	
	public static void clearEntities(World world, Location betweenHere, Location andHere, Class... onlyThese)
	{
		List<Entity> entList = world.getEntities();//get all entities in the world
		if ((betweenHere != null && andHere != null) && (!betweenHere.getWorld().equals(world) || !andHere.getWorld().equals(world))) {throw new IllegalArgumentException("Sorry, but the two locations need to be of the same world as the world parameter.");}
		
		System.out.println("Checking for Entities between "+betweenHere+" and "+andHere+".");
        for(Entity current : entList)
        {//loop through the list
        	
        	for (Class classs: onlyThese)
        	{
        		System.out.println("Found "+classs.getSimpleName()+" entity at "+current.getLocation()+"!!");
	            if (classs.isInstance(current))
	            {//make sure we aren't deleting mobs/players
	            	
	            	if (UtilityFunctions.isABetweenBAndC(current.getLocation(), betweenHere, andHere))
	            	{
	            		System.out.println("Found Entity in valid location!! Removing.");
	            		current.remove();//remove it
	            	}
	            }
        	}
        }
	}
	
	public static Villager spawnVillager(Location location, String name)
	{
		Villager v = (Villager) location.getWorld().spawnEntity(location, EntityType.VILLAGER);
        v.setCustomName(name);
        return v;
	}
}
