package com.chaoscrasher.spigotutility.all.listeners;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.chaoscrasher.spigotutility.all.io.PlayerDataManagement;
import com.chaoscrasher.spigotutility.all.io.ServerDataManagement;

/**
 * 
 * @author Chaoscrasher
 * Exists for the purpose of providing general functionality, like allowing for a custom spawn point.
 */
public class GeneralListener implements Listener
{
	public static boolean DEBUG = false;
	
	/**
	 * sets the respawn location to their one time repsawn location, or if that doesn't exist
	 * to their permanent respawn location, or if that doesn't exist, the custom server spawn location.
	 * 
	 * @param e PlayerRespawnEvent
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerRespawn(PlayerRespawnEvent e)
	{
		Location rsLoc = PlayerDataManagement.getRespawnLocation(e.getPlayer());
		Location otLoc = PlayerDataManagement.getAndClearOneTimeRespawnLocation(e.getPlayer());
		Location bsLoc = e.getPlayer().getBedSpawnLocation();
		if (DEBUG) System.out.println("Player "+e.getPlayer()+" has "+rsLoc+" as respawn point.");
		if (otLoc != null || rsLoc != null) e.setRespawnLocation(otLoc != null ? otLoc : rsLoc != null ? rsLoc : bsLoc != null ? bsLoc : ServerDataManagement.ref.getServerSpawnPoint());
	}
}
