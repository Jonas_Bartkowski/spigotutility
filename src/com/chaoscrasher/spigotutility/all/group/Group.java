package com.chaoscrasher.spigotutility.all.group;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.bukkit.entity.Player;

public class Group 
{
	private Player leader;
	private List<Player> groupMembers = new ArrayList();

	public Group(Collection<Player> groupMembers, Player leader)
	{
		if (groupMembers.contains(leader))
		{
			setLeader(leader);
		}
		throw new IllegalArgumentException("Suggested group leader "+leader.getName()+" is not part of group collection.");
	}
	
	public Player getLeader() {
		return leader;
	}

	public void setLeader(Player leader) {
		this.leader = leader;
		addMember(leader);
	}

	public List<Player> getGroupMembers() {
		return groupMembers;
	}

	public void setGroupMembers(List<Player> groupMembers) {
		this.groupMembers = groupMembers;
	}
	
	public boolean addMember(Player player)
	{
		return (!containsMember(player) && groupMembers.add(player));
	}
	
	public boolean addMembers(Collection<Player> members)
	{
		return (!containsMembers(members) && groupMembers.addAll(members));
	}
	
	public boolean removeMember(Player player)
	{
		return groupMembers.remove(player);
	}
	
	public boolean containsMember(Player member)
	{
		return groupMembers.contains(member);
	}
	
	public boolean containsMembers(Collection<Player> members)
	{
		return groupMembers.containsAll(members);
	}
	
	public int getGroupSize()
	{
		return groupMembers.size();
	}
	
	public static Group getPlayerGroup(Player player)
	{
		throw new NotImplementedException();
	}
}
