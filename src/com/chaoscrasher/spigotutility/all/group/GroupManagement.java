package com.chaoscrasher.spigotutility.all.group;

import java.util.HashMap;

import org.bukkit.entity.Player;

public class GroupManagement 
{
	private static HashMap<Player, Group> leaderGroupMap = new HashMap<Player, Group>();
	
	public Group getGroup(Player leader)
	{
		return leaderGroupMap.get(leader);
	}
	
	public void addGroup(Group group)
	{
		leaderGroupMap.put(group.getLeader(), group);
	}
}
