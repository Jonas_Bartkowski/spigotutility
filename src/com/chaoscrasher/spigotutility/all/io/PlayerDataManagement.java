package com.chaoscrasher.spigotutility.all.io;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.chaoscrasher.spigotutility.all.data.LocationString;
import com.chaoscrasher.spigotutility.all.data.Saveable;

public class PlayerDataManagement implements Saveable
{
	public static final String STD_KEY_VALUE_SEPERATOR = "=";
	public static final String STD_VALUE_SEPERATOR = ";";
	public static final String STD_NEXT_HIT_APPLIES_KEY = "nextHitApplies";
	public static final String STD_RESPAWN_LOCATION_KEY = "respawnLocation";
	public static final String STD_ONE_TIME_RESPAWN_LOCATION_KEY = "OneTimerespawnLocation";
	public static final String STD_PRE_PORT_LOCATION_KEY = "preportloc";
	
	private static HashMap<UUID, HashMap<String, List<String>>> allPlayerData = new HashMap<UUID, HashMap<String, List<String>>>();
	
	/**
	 * Provides easy functionality to set which data is supposed to be applied on hit.
	 * @param player The player that is supposed to apply data on hit
	 * @param data The data that player is supposed to apply on hit.
	 * @param deleteOther Whether previous data is supposed to be overwritten
	 * @param addTo Whether data is just supposed to be added to previous data.
	 */
	public static void setNextHitApplies(Player player, String data, boolean deleteOther, boolean addTo)
	{
		List<String> list = new ArrayList();
		list.add(data);
		setNextHitApplies(player, list, deleteOther, addTo);
	}
	
	/**
	 * Provides easy functionality to set which data is supposed to be applied on hit.
	 * @param player The player that is supposed to apply data on hit
	 * @param data The data that player is supposed to apply on hit.
	 * @param deleteOther Whether previous data is supposed to be overwritten
	 * @param addTo Whether data is just supposed to be added to previous data.
	 */
	public static void setNextHitApplies(Player player, List<String> data, boolean overwrite, boolean addTo)
	{
		setStateData(player, STD_NEXT_HIT_APPLIES_KEY, data, overwrite, addTo);
	}
	
	/**
	 * Provides easy functionality to set which data is supposed to be applied on hit.
	 * @param player The player that is supposed to apply data on hit
	 * @param data The data that player is supposed to apply on hit.
	 * @param deleteOther Whether previous data is supposed to be overwritten
	 * @param addTo Whether data is just supposed to be added to previous data.
	 */
	public static void setNextHitApplies(Player player, boolean overwrite, boolean addTo, String... data)
	{
		setStateData(player, STD_NEXT_HIT_APPLIES_KEY, overwrite, addTo, data);
	}
	 
	/**
	 * Gets the data that is applied on next hit for a certain player.
	 * @param player the players whose on hit data is read
	 * @return
	 */
	public static List<String> getNextHitApplies(Player player)
	{
		return getStateData(player, STD_NEXT_HIT_APPLIES_KEY);
	}
	
	public static void clearNextHitApplies(Player player)
	{
		clearPlayerStateData(player, STD_NEXT_HIT_APPLIES_KEY);
	}
	
	public static void setRespawnLocation(Player player, Location location, boolean oneTimeOnly)
	{
		System.out.println("Trying to set respawn location for "+player.getName()+" to "+location);
		if (oneTimeOnly)
		{
			setLocationForKey(player, STD_ONE_TIME_RESPAWN_LOCATION_KEY, location);
			System.out.println("One time respawn location is now "+getLocationFromKey(player, STD_ONE_TIME_RESPAWN_LOCATION_KEY));
		}
		else
		{
			setLocationForKey(player, STD_RESPAWN_LOCATION_KEY, location);
			System.out.println("Respawn location is now "+getRespawnLocation(player));
		}	
	}
	
	public static Location getRespawnLocation(Player player)
	{
		return getLocationFromKey(player, STD_RESPAWN_LOCATION_KEY);
	}
	
	public static Location getAndClearOneTimeRespawnLocation(Player player)
	{
		Location loc = getLocationFromKey(player, STD_ONE_TIME_RESPAWN_LOCATION_KEY);
		if (loc != null)
			clearPlayerStateData(player, STD_ONE_TIME_RESPAWN_LOCATION_KEY);
		
		return loc;
	}

	public static void clearRespawnLocation(Player player)
	{
		clearPlayerStateData(player, STD_RESPAWN_LOCATION_KEY);
	}
	
	public static void setPrePortLocation(Player player, Location location)
	{
		setLocationForKey(player, STD_PRE_PORT_LOCATION_KEY, location);
	}
	
	public static Location getPrePortLocation(Player player)
	{
		return getLocationFromKey(player, STD_PRE_PORT_LOCATION_KEY);
	}
	
	public static void clearPrePortLocation(Player player)
	{
		clearPlayerStateData(player, STD_PRE_PORT_LOCATION_KEY);
	}
	
	public static void setLocationForKey(Player player, String key, Location location)
	{
		List<String> datal = new ArrayList<>();
		datal.add((new LocationString(location)).toString());
		setStateData(player, key, datal, true, false);
	}
	
	public static Location getLocationFromKey(Player player, String key)
	{
		List<String> data = getStateData(player, key);
		if (data != null)
		{
			try
			{
				return (LocationString.locationFromString(data.get(0)));
			}
			catch (IllegalArgumentException e)
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	public static void setStateData(Player player, String stateName, String stateValue, boolean overwrite, boolean addTo)
	{
		List<String> list = new ArrayList();
		list.add(stateValue);
		setStateData(player, stateName, list, overwrite, addTo);
	}
	
		public static void setStateData(Player player, String stateName, List<String> stateValues, boolean overwrite, boolean addTo) 
		{
			HashMap<String, List<String>> singlePlayerData = allPlayerData.get(player.getUniqueId());
			if (singlePlayerData != null)
			{		
				if (overwrite)
				{
					singlePlayerData.put(stateName, stateValues);
				}
				else if (addTo)
				{				
					List<String> singleValue = singlePlayerData.get(stateName);
					if (singleValue != null)
					{
						singleValue.addAll(stateValues);
					}
					else
					{
						setStateData(player, stateName, stateValues, true, false);
					}
				}
			}
			else
			{
				HashMap<String, List<String>> newStateNameValueMap = new HashMap<String, List<String>>();
				newStateNameValueMap.put(stateName, stateValues);
				allPlayerData.put(player.getUniqueId(), newStateNameValueMap);
			}
		}
		
		public static void setStateData_overwrite(Player player, String stateName, List<String> stateValues) 
		{
			setStateData(player, stateName, stateValues, true, false);
		}
		
	public static void setStateData(Player player, String stateName, boolean overwrite, boolean addTo, String... stateValues)
	{
		List<String> datal = new ArrayList<>();
		for (String dp: stateValues)
			datal.add(dp);
		
		setStateData(player, stateName, datal, overwrite, addTo);
	}
		
		public static void setStateData_overwrite(Player player, String stateName, String... stateValues)
		{
			setStateData(player, stateName, true, false, stateValues);
		}
	
	public static boolean setStateData_quick(Player player, String data, boolean overwrite, boolean addTo)
	{
		String[] da = data.split(STD_KEY_VALUE_SEPERATOR);
		if (da != null && da.length >= 2)
		{
			String key = da[0];
			String[] values = da[1].split(STD_VALUE_SEPERATOR);
			
			setStateData(player, key, overwrite, addTo, values);
			return true;
		}
		else
			return false;
	}
			
		public static void setStateData_quick(Player player, boolean overwrite, boolean addTo, String... stateData)
		{
			for (String s: stateData)
			{
				setStateData_quick(player, s, overwrite, addTo);
			}
		}
			
		public static void setStateData_quick(Player player, String... stateData)
		{
			for (String s: stateData)
			{
				setStateData_quick(player, s, true, false);
			}
		}
		
		public static void setStateData_quick_overwrite(Player player, String data)
		{
			setStateData_quick(player, data, true, false);
		}
	
	
	public static List<String> getStateData(Player player, String stateName)
	{
		HashMap<String, List<String>> pdata = allPlayerData.get(player.getUniqueId()); 
		if (pdata != null)
			return pdata.get(stateName);
		
		return null;
	}
	
	public static HashMap<String, List<String>> getAllStateData(Player player)
	{
		return allPlayerData.get(player.getUniqueId());
	}
	
	public static String getFirstElementOfStateData(Player player, String stateName)
	{
		return getStateData(player, stateName).get(0);
	}
	
	public static void clearPlayerData(Player player)
	{
		allPlayerData.remove(player.getUniqueId());
	}
	
	public static void clearPlayerStateData(Player player, String stateName)
	{
		getAllStateData(player).remove(stateName);
	}
	
	public static void printMap()
	{
		System.out.println("Found "+allPlayerData.size()+" player entries");
		for (UUID player: allPlayerData.keySet())
		{
			System.out.println(Bukkit.getPlayer(player)+": ");
			HashMap<String, List<String>> pdata = allPlayerData.get(player);
			for (String states: pdata.keySet())
			{
				System.out.print(states+"(");
				for (String val: pdata.get(states))
				{
					System.out.print(val+";");
				}
				System.out.print(")");
			}
		}
	}
	
	@Override
	public boolean save()
	{
		try 
		{
			FileOutputStream fos = new FileOutputStream("pmap.ser");
	        ObjectOutputStream oos = new ObjectOutputStream(fos);
	        oos.writeObject(allPlayerData);
	        oos.close();
	        return true;
		}
		catch (IOException e) {System.err.println("Couldn't save pmap.ser"); return false;}
	}
	
	@Override
	public boolean load()
	{
		boolean ret = false;
		try
		{
			FileInputStream fis = new FileInputStream("pmap.ser");
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        allPlayerData = (HashMap<UUID, HashMap<String, List<String>>>) ois.readObject();
	        ois.close();
	        ret = true;
		}
		catch (FileNotFoundException e) 
		{
			System.err.println("Couldn't find pmap.ser.");
		} 
		catch (IOException e) 
		{
			System.err.println("Couldn't find pmap.ser.");
		} 
		catch (ClassNotFoundException e)
		{
			System.err.println("Target file is corrupted."); 
		}
		return ret;
	}
	
	public static void setUUIDForKey(Player player, String key)
	{
		setStateData(player, key, player.getUniqueId().toString(), true, false);
	}
	
	public static String getStringFromKey(Player player, String key, boolean ignoreIfNotSizeOne)
	{
		List<String> entries = getStateData(player, key);
		if (entries != null && !entries.isEmpty() && (entries.size() == 1 || !ignoreIfNotSizeOne))
			return entries.get(0);
		else
			return null;
	}
	
	public static String getStringFromKey(Player player, String key)
	{
		return getStringFromKey(player, key, false);
	}
	
	public static UUID getUUIDFromKey(Player player, String key)
	{
		String uuid = getStringFromKey(player, key);
		try {
			return UUID.fromString(uuid);
		}
		catch (Exception e)
		{
			return null;
		}
	}
	
	public static void clearAll()
	{
		allPlayerData.clear();
	}
}
