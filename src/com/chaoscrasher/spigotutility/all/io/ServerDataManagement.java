package com.chaoscrasher.spigotutility.all.io;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;

import com.chaoscrasher.spigotutility.all.data.LocationString;
import com.chaoscrasher.spigotutility.all.data.Saveable;

public class ServerDataManagement implements Saveable
{
	public static ServerDataManagement ref;
	
	private static FileConfiguration config;
	private static String CONFIG_PATH = "config.yaml";
	
	public String SPAWN_KEY = "spawn";
	private Location serverSpawnPoint = Bukkit.getServer().getWorlds().get(0).getSpawnLocation();
	
	public Location getServerSpawnPoint()
	{
		return serverSpawnPoint;
	}
	
	public void setServerSpawnPoint(Location location)
	{
		serverSpawnPoint = location;
	}
	
	@Override
	public boolean load()
	{
		boolean ret = false;
		try 
		{
			config.load(CONFIG_PATH);
			Object o = config.get("spawn");
			if (o instanceof LocationString)
			{ 
				serverSpawnPoint = ((LocationString) o).getLocation();
				System.out.println("Spawn = "+serverSpawnPoint+" successfully load.");
			}
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			System.err.println("ERROR: Could not load config.");
		}
		return ret;
	}
	
	@Override
	public boolean save()
	{
		boolean ret = false;
		config.set(SPAWN_KEY, new LocationString(serverSpawnPoint));
		try 
		{
			config.save(CONFIG_PATH);
			ret = true;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			System.err.println("ERROR: could not save config.");
		}
		return ret;
	}
	
	public static void supplyConfig(FileConfiguration config)
	{
		ServerDataManagement.config = config;
	}
}
