package com.chaoscrasher.spigotutility.all.data;

import java.io.Serializable;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.material.Bed;


public class BedLocation implements Serializable
{
	private LocationString headLocationString;
	private BlockFace facing;
	
	public BedLocation(Location bedHeadLocation, BlockFace facing)
	{
		if (bedHeadLocation != null)
			this.headLocationString = new LocationString(bedHeadLocation);
		else
			throw new IllegalArgumentException("Cannot create location String with null as Location.");
		if (checkFacing(facing))
			this.facing = facing;
	}
	
	public BlockFace getFacing() 
	{
		return facing;
	}
	
	public void setFacing(BlockFace facing) 
	{
		if (checkFacing(facing))
			this.facing = facing;
	}
	
	public Location getHeadLocation()
	{
		return headLocationString.getLocation();
	}
	
	public Location getFootLocation()
	{
		Block headBlock = getHeadLocation().getBlock();
		try
		{
			return getTwinLocation(((Bed)headBlock.getState().getData()), headBlock);
		}
		catch (ClassCastException e)
		{
			return null;
		}
	}
	
	public Bed getBed()
	{
		return ((Bed) getHeadLocation().getBlock().getState().getData());
	}
	
	private static boolean checkFacing(BlockFace facing)
	{
		if (!facing.equals(BlockFace.NORTH) && !facing.equals(BlockFace.EAST) && !facing.equals(BlockFace.SOUTH) && !facing.equals(BlockFace.WEST))
		{
			throw new IllegalArgumentException("Sorry, but facing '"+facing+"' is invalid. Only NORTH, EAST, SOUTH, WEST are allowed.");
		}
		return true;
	}
	
	private static Location getTwinLocation(Bed b, Block bloc)
	{
        if(b.isHeadOfBed())
        {
            return bloc.getRelative(b.getFacing().getOppositeFace()).getLocation();
        }
        else
        {
            return bloc.getRelative(b.getFacing()).getLocation();
        }
    }
	
}


