package com.chaoscrasher.spigotutility.all.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationString implements Serializable
{
	private String locationString;
	
	private LocationString(String locStr)
	{
		this.locationString = locStr;
	}
	
	public LocationString(Location loc)
	{
		this(locationToString(loc));
	}
	
	public LocationString(String world, int x, int y, int z)
	{
		this(world+";"+x+";"+y+";"+z+";");
	}
	
	public Location getLocation()
	{
		return locationFromString(locationString);
	}
	
	public static String locationToString(Location loc)
	{
		if (loc == null)
			return null;
		
		return loc.getWorld().getName()+";"+loc.getX()+";"+loc.getY()+";"+loc.getZ()+";";
	}
	
	public static Location locationFromString(String locStr)
	{
		if (locStr == null)
			return null;
		
		String[] data = locStr.split(";");
		
		if (data.length == 4)	
			try {return new Location(Bukkit.getWorld(data[0]), Double.valueOf(data[1]), Double.valueOf(data[2]), Double.valueOf(data[3]));} 
		    catch (NumberFormatException e) {throw new IllegalArgumentException("Sorry, but "+locStr+" is no valid location string!");}
		else
			throw new IllegalArgumentException("Sorry, but "+locStr+" is no valid location string!");	
	}
	
	public static List<LocationString> locCollectionToLocStrList(Collection<Location> locations)
	{
		List<LocationString> ret = new ArrayList();
		for (Location loc: locations)
		{ 
			ret.add(new LocationString(loc));
		}
		return ret;
	}	
	
	public static List<Location> locStrCollectionLocList(Collection<LocationString> locationStrings)
	{
		List<Location> ret = new ArrayList();
		for (LocationString locStr: locationStrings)
		{ 
			ret.add(locStr.getLocation());
		}
		return ret;
	}	
	
	public String toString()
	{
		return locationString;
	}
}
