package com.chaoscrasher.spigotutility.all.data;

import java.io.IOException;

public interface Saveable
{
	public boolean save();
	public boolean load();
}