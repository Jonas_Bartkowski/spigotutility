package com.chaoscrasher.spigotutility.all.commandexecutors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public abstract class ECommand extends Command {

    private final static String NO_PERMISSION = ChatColor.RED + "You do not have permission to execute this command.";
    private final static String NO_PLAYER = ChatColor.RED + "This command can only be done in game.";
    private final static String NOT_ENOUGH_ARGS = ChatColor.RED + "Incorrect usage! /%name% %usage%";

    private boolean playerRequired = false;
    private int minimumArgs = 0;

    public ECommand(String name) {
        super(name);
        super.setPermissionMessage(NO_PERMISSION);
        super.setUsage("");
    }

    public boolean isPlayerRequired() {
        return playerRequired;
    }

    public void setPlayerRequired(boolean playerRequired) {
        this.playerRequired = playerRequired;
    }

    public int getMinimumArgs() {
        return minimumArgs;
    }

    public void setMinimumArgs(int minimumArgs) {
        this.minimumArgs = minimumArgs;
    }

    public final void register() {
        try {
            final Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
            commandMap.register(getName(), this);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (isPlayerRequired() && !(sender instanceof Player)) {
            sender.sendMessage(NO_PLAYER);
            return true;
        }
        if (!getPermission().equals("") && !sender.hasPermission(getPermission())) {
            sender.sendMessage(NO_PERMISSION);
            return true;
        }
        if (args.length < getMinimumArgs()) {
            sender.sendMessage(NOT_ENOUGH_ARGS.replace("%name%", getName()).replace("%usage%", getUsage()));
            return true;
        }
        if (!onCommand(sender, args)) {
            sender.sendMessage(NOT_ENOUGH_ARGS.replace("%name%", getName()).replace("%usage%", getUsage()));
        }
        return true;
    }
    
    public abstract boolean onCommand(CommandSender sender, String[] arguments);
}
