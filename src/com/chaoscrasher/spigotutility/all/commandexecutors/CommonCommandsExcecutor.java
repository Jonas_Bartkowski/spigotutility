package com.chaoscrasher.spigotutility.all.commandexecutors;

import static com.chaoscrasher.spigotutility.all.commandexecutors.CommandExcecutorFunctions.sendGreen;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.chaoscrasher.spigotutility.all.io.ServerDataManagement;

import static com.chaoscrasher.spigotutility.all.commandexecutors.CommandExcecutorFunctions.*;

public class CommonCommandsExcecutor implements CommandExecutor
{
	public static String SET_SPAWN_CMD = "setspawn";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if (sender.isOp())
		{
			if (args.length == 1)
			{
				if (args[0].equalsIgnoreCase(SET_SPAWN_CMD))
				{
					if (sender instanceof Player)
					{
						Player player = (Player) sender;
						sendGreen(sender, "Spawn was set");
						ServerDataManagement.ref.setServerSpawnPoint(player.getLocation());
					}
					else
					{
						onlyPlayersAllowed(sender);
					}		
					return true;
				}
			}
			return false;
		}
		sender.sendMessage(ChatColor.RED+"FORBIDDEN");
		return true;
	}

}
