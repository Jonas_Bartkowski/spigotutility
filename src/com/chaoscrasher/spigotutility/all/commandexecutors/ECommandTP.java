package com.chaoscrasher.spigotutility.all.commandexecutors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ECommandTP extends ECommand {

    public ECommandTP() 
    {
        super("ectp");
        super.setPermission("admin");
        super.setDescription("tps you to another world");
        super.setUsage("<world> <x> <y> <z>");
        super.setMinimumArgs(4);
        super.register();
    }

    @Override
    public boolean onCommand(CommandSender sender, String[] arguments) 
    {
    	if (sender instanceof Player)
    	{
	        Player player = (Player) sender;
	        World w = Bukkit.getWorld(arguments[0]);
	        if (w != null)
	        {
	        	try
	        	{
	        		double x = Double.valueOf(arguments[1]);
	        		double y = Double.valueOf(arguments[2]);
	        		double z = Double.valueOf(arguments[3]);
		        	Location location = new Location(Bukkit.getWorld(arguments[0]), Double.valueOf(arguments[1]), Double.valueOf(arguments[2]), Double.valueOf(arguments[3]));
			     	player.sendMessage(ChatColor.GREEN+"Teleporting you...");
		        	player.teleport(location);
			     	
	        	}
	        	catch (NumberFormatException e)
	        	{
	        		player.sendMessage(ChatColor.RED+"Sorry, but you didn't enter valid x y or z values. (double or int");
	        		return false;
	        	}
	        }
	     	else
	     	{
	     		player.sendMessage(ChatColor.RED+"World "+arguments[0]+" doesn't exist!\nThese are valid maps:\n");
	     		for (World world: Bukkit.getWorlds())
	     		{
	     			player.sendMessage(world.getName());
	     		}
	     	}
	     	return true;
    	}
    	sender.sendMessage("This can only be done by a player.");
    	return true;
    }
}
