package com.chaoscrasher.spigotutility.all.commandexecutors;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class CommandExcecutorFunctions 
{
	public static void onlyPlayersAllowed(CommandSender sender)
	{
		sender.sendMessage(ChatColor.RED+"Only a player can do this.");
	}
	
	public static void sendInColor(CommandSender sender, String msg, ChatColor color)
	{
		sender.sendMessage(color+msg);
	}
	
	public static void sendGreen(CommandSender sender, String msg)
	{
		sendInColor(sender, msg, ChatColor.GREEN);
	}
	
	public static void sendRed(CommandSender sender, String msg)
	{
		sendInColor(sender, msg, ChatColor.RED);
	}
	
	public static void sendOrange(CommandSender sender, String msg)
	{
		sendInColor(sender, msg, ChatColor.YELLOW);
	}
}
